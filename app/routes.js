const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({ 'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	});

	app.post('/currency', (req, res) => {
		//create if conditions here to check the currency

		// 2. Check if post /currency returns status 400 if name is missing
		if (!req.body.hasOwnProperty('name')) return res.status(400).send({ "Error": "Bad request - missing required parameter name" });

		// 3. Check if post /currency returns status 400 if name is not a string
		if (typeof (req.body.name) !== 'string') return res.status(400).send({ "Error": "Bad request -  parameter name not a string" });

		// 4. Check if post /currency returns status 400 if name is empty
		if (req.body.name === '') return res.status(400).send({ "Error": "Bad request -  parameter name empty" });

		// 5. Check if post /currency returns status 400 if ex is missing
		if (!req.body.hasOwnProperty('ex')) return res.status(400).send({ "Error": "Bad request - missing required parameter ex" });

		// 6. Check if post /currency returns status 400 if ex is not an object
		if (typeof (req.body.ex) !== 'object') return res.status(400).send({ "Error": "Bad request -  parameter ex not an object" });

		// 7. Check if post /currency returns status 400 if ex is empty
		if (Object.keys(req.body.ex).length === 0) return res.status(400).send({ "Error": "Bad request -  parameter ex empty" });

		// 8. Check if post /currency returns status 400 if alias is missing
		if (!req.body.hasOwnProperty('alias')) return res.status(400).send({ "Error": "Bad request - missing required parameter alias" });

		// 9. Check if post /currency returns status 400 if alias is not an string
		if (typeof (req.body.alias) !== 'string') return res.status(400).send({ "Error": "Bad request -  parameter alias not a string" });

		// 10. Check if post /currency returns status 400 if alias is empty
		if (req.body.alias === '') return res.status(400).send({ "Error": "Bad request -  parameter alias empty" });

		// 11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
		if (req.body.alias in exchangeRates) return res.status(400).send({ "Error": "Bad request -  duplicate alias" });

		// 1. Check if post /currency is running (status 200)
		// 12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		});
	});
}
