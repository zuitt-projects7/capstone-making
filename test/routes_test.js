const { assert } = require('chai');
const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

const domain = 'http://localhost:5001';

describe('forex_api_test_suite', (done) => {
	//add the solutions here

	// 1. Check if post /currency is running (status 200)
	// 12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
	it('POST /currency endpoint', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: 'Saudi Arabian Riyadh',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status, 200);
				done();
			});
	});

	// 2. Check if post /currency returns status 400 if name is missing
	it('POST /currency endpoint to check if name is missing', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 3. Check if post /currency returns status 400 if name is not a string
	it('POST /currency endpoint to check if name is not a string', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: 2351996,
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 4. Check if post /currency returns status 400 if name is empty
	it('POST /currency endpoint to check if name is empty', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: '',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 5. Check if post /currency returns status 400 if ex is missing
	it('POST /currency endpoint to check if ex is missing', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: 'Saudi Arabian Riyadh',
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 6. Check if post /currency returns status 400 if ex is not an object
	it('POST /currency endpoint to check if ex is not an object', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: 'Saudi Arabian Riyadh',
				ex: false
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 7. Check if post /currency returns status 400 if ex is empty
	it('POST /currency endpoint to check if ex is empty', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: 'Saudi Arabian Riyadh',
				ex: {}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 8. Check if post /currency returns status 400 if alias is missing
	it('POST /currency endpoint to check if alias is missing', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				name: 'Saudi Arabian Riyadh',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 9. Check if post /currency returns status 400 if alias is not a string
	it('POST /currency endpoint to check if alias is not a string', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: [
					['Zuitt', 'Coding', 'Bootcamp'],
					['Batch', 143]
				],
				name: 'Saudi Arabian Riyadh',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 10. Check if post /currency returns status 400 if alias is empty
	it('POST /currency endpoint to check if alias is empty', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'riyadh',
				name: '',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});

	// 11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
	it('POST /currency endpoint', (done) => {
		chai.request(domain)
			.post('/currency')
			.type('json')
			.send({
				alias: 'won',
				name: 'South Korean Won',
				ex: {
					'peso': 0.043,
					'usd': 0.00084,
					'yen': 0.092,
					'yuan': 0.0059
				}
			})
			.end((err, res) => {
				expect(res.status).to.equal(400);
				done();
			});
	});
})
